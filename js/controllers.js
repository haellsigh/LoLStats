'use strict';

/* Controllers */

var ConstantsSummaryType = {
    "Unranked": "Summoner's Rift unranked games",
    "Unranked3x3": "Twisted Treeline unranked games",
    "OdinUnranked": "Dominion/Crystal Scar games",
    "AramUnranked5x5": "ARAM / Howling Abyss games",
    "CoopVsAI": "Summoner's Rift and Crystal Scar games played against AI",
    "CoopVsAI3x3": "Twisted Treeline games played against AI",
    "RankedSolo5x5": "Summoner's Rift ranked solo queue games",
    "RankedTeam3x3": "Twisted Treeline ranked team games",
    "RankedTeam5x5": "Summoner's Rift ranked team games",
    "OneForAll5x5": "One for All games",
    "FirstBlood1x1": "Snowdown Showdown 1x1 games",
    "FirstBlood2x2": "Snowdown Showdown 2x2 games",
    "SummonersRift6x6": "Hexakill games",
    "CAP5x5": "Team Builder games",
    "URF": "Ultra Rapid Fire games",
    "URFBots": "Ultra Rapid Fire games played against AI"
}

var ConstantsSubType = {
    "NONE":					"Custom game",
    "NORMAL":				"Summoner's Rift unranked game",
    "NORMAL_3x3":			"Twisted Treeline unranked game",
    "ODIN_UNRANKED":		"Dominion/Crystal Scar game",
    "ARAM_UNRANKED_5x5":	"ARAM / Howling Abyss game",
    "BOT":					"Summoner's Rift and Crystal Scar game played against AI",
    "BOT_3x3":				"Twisted Treeline game played against AI",
    "RANKED_SOLO_5x5":		"Summoner's Rift ranked solo queue game",
    "RANKED_TEAM_3x3":		"Twisted Treeline ranked team game",
    "RANKED_TEAM_5x5":		"Summoner's Rift ranked team game",
    "ONEFORALL_5x5":		"One for All game",
    "FIRSTBLOOD_1x1":		"Snowdown Showdown 1x1 game",
    "FIRSTBLOOD_2x2":		"Snowdown Showdown 2x2 game",
    "SR_6x6":				"Hexakill game",
    "CAP_5x5":				"Team Builder game",
    "URF":					"Ultra Rapid Fire game",
    "URF_BOT":				"Ultra Rapid Fire game played against AI"
}


var ConstantsMapsId = {
    "1":	"Summoner's Rift",
    "2":	"Summoner's Rift",
    "3":	"The Proving Grounds",
    "4":	"Twisted Treeline",
    "8":	"The Crystal Scar",
    "10":	"Twisted Treeline",
    "12":	"Howling Abyss"
}

var lolControllers = angular.module('lolControllers', ['ui.bootstrap']);

lolControllers.controller('SummonerDetailCtrl', ['$scope', '$routeParams', 'Summoner', 'Summary', 'LeagueEntry', 'Games', 
    function($scope, $routeParams, Summoner, Summary, LeagueEntry, Games) {
        $scope.summonername = $routeParams.summonerName.toLowerCase().replace(/ /g, '');
        Summoner.query({
            summonerName: $scope.summonername
        }, function(value, responseHeaders) {
            console.log(value);
            $scope.summoner = value[$scope.summonername];
            $scope.summonerencodedid = value[$scope.summonername].id;
            
            ////////////
            Summary.query({
                summonerId: value[$scope.summonername].id
            }, function(val, respHeads) {
                $scope.summary = val.playerStatSummaries;
                $scope.summary.totalWins = 0;
                var modder = 0;
                
                angular.forEach($scope.summary, function(value, key) {
                    if(value.playerStatSummaryType === 'RankedPremade3x3' || value.playerStatSummaryType === 'RankedPremade5x5') {
                        $scope.summary.splice(key - modder, 1);
                        modder++;
                    }
                    $scope.summary.totalWins += value.wins;
                });
             ////////////
             ////////////
                LeagueEntry.query({
                    summonerId: $scope.summonerencodedid
                }, function(leagues, rHeaders) {
                    $scope.league = leagues[$scope.summonerencodedid];
                    console.log($scope.league);
                    $scope.currentDivisionText = $scope.league[0].tier.toLowerCase() + ' ' + $scope.league[0].entries[0].division;
                    $scope.currentDivision = $scope.league[0].tier + '_' + $scope.league[0].entries[0].division;
                }, function(httpError) {
                    if(httpError.status == 404) $scope.currentDivisionText = 'Unranked';
                    $scope.currentDivision = 'UNRANKED'
                })
             ////////////
             ////////////
                Games.query({
                    summonerId: $scope.summonerencodedid
                }, function(gamesret, rHeaders) {
                    $scope.games = gamesret.games; //array
                    $scope.status;
                    
                    angular.forEach($scope.games, function(game, key) {
                        $scope.status[key] = {open: false};
                    })
                })
             ////////////
            })
        });
        $scope.ConstantsSummaryType = ConstantsSummaryType;
        $scope.ConstantsMapsId = ConstantsMapsId;
        $scope.ConstantsSubType = ConstantsSubType;
        
        $scope.status = {open: false};
    }
]);
lolControllers.controller('ItemListCtrl', ['$scope', '$routeParams', 'Items',
    function($scope, $routeParams, Items) {}
]);
lolControllers.controller('ItemDetailCtrl', ['$scope', '$routeParams', 'Item',
    function($scope, $routeParams, Item) {}
]);