'use strict';

/* App Module */

var lolApp = angular.module('lolApp', ['ngRoute', 'lolControllers', 'lolServices', ]);
                                       
lolApp.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.

        // SUMMONER

        when('/summoner/:summonerName', {
            templateUrl: 'partials/summoner/detail.html',
            controller: 'SummonerDetailCtrl'
        }).

        // CHAMPION

        when('/champion/', {
            templateUrl: 'partials/champion/list.html',
            controller: 'ChampionListCtrl'
        }).
        when('/champion/:championId', {
            templateUrl: 'partials/champion/detail.html',
            controller: 'ChampionDetailCtrl'
        }).

        // ITEM

        when('/item/', {
            templateUrl: 'partials/item/list.html',
            controller: 'ItemListCtrl'
        }).
        when('/item/:itemId', {
            templateUrl: 'partials/item/detail.html',
            controller: 'ItemDetailCtrl'
        }).


        otherwise({
            redirectTo: '/summoner/default'
        });
    }
]);