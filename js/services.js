'use strict';
/* Services */
var apikey = '270303ae-2637-4496-ad31-2b448541dafa';
var region = 'euw';
var endpoint = region + '.api.pvp.net';

var lolServices = angular.module('lolServices', ['ngResource']);

lolServices.factory('Summoner', ['$resource',
    function($resource) {
        return $resource('https://' + endpoint + '/api/lol/' + region + '/v1.4/summoner/by-name/:summonerName?api_key=' + apikey, {}, {
            query: {
                method: 'GET',
                params: {
                    summonerName: 'default'
                },
                isArray: false
            }
        });
    }
]);


lolServices.factory('Summary', ['$resource',
    function($resource) {
        return $resource('https://' + endpoint + '/api/lol/' + region + '/v1.3/stats/by-summoner/:summonerId/summary?api_key=' + apikey, {}, {
            query: {
                method: 'GET',
                params: {
                    summonerId: '0'
                },
                isArray: false
            }
        });
    }
]);


lolServices.factory('LeagueEntry', ['$resource',
    function($resource) {
        return $resource('https://' + endpoint + '/api/lol/' + region + '/v2.4/league/by-summoner/:summonerId/entry?api_key=' + apikey, {}, {
            query: {
                method: 'GET',
                params: {
                    summonerId: '0'
                },
                isArray: false
            }
        });
    }
]);

lolServices.factory('Games', ['$resource',
    function($resource) {
        return $resource('https://' + endpoint + '/api/lol/' + region + '/v1.3/game/by-summoner/:summonerId/recent?api_key=' + apikey, {}, {
            query: {
                method: 'GET',
                params: {
                    summonerId: '0'
                },
                isArray: false
            }
        });
    }
]);